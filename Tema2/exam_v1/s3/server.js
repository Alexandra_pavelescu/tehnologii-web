const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/update/:id', (req, res) => {
     if(req.body.productName && req.body.price){
        let product = {
            id: req.params.id,
            productName: req.body.productName,
            price: req.body.price
        };
       for(var i=0;i<products.length;i++){
           if(products[i].id == product.id){
              // products[i].productName=product.productName;
              // products[i].price=product.price;
              products[i]=product;
           }
       }
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
     
});

app.delete('/delete', (req, res) => {
    if(req.body.productName){
        let product = req.body.productName;
       /* var i=0;
        while(i<products.length){
            if(products[i].productName === product){
                delete products[i];
            }
            else{
                i++;
            }
        }*/
        for(var i=0;i<products.length;i++){
            if(product[i].productName == product){
                products.splice(i,1);
               
            }
        }
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});


app.listen(8080, () => {
    console.log('Server started on port 8080...');
});