import React from 'react';
import axios from 'axios';

export class AddProduct extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.id = 0
        this.state.productName = ""
        this.state.price = 0;
    }
    
     handleChangeId = (event) => {
        this.setState({
            id: event.target.value
        })
    }
    
    handleChangeProductName = (event) => {
        this.setState({
            productName: event.target.value
        })
    }
    
    handleChangePrice = (event) => {
        this.setState({
            price: event.target.value
        })
    }
    
    
    handleAddClick = () => {
        let product = {
            id: this.state.id,
            productName: this.state.productName,
            price: this.state.price
        }
        axios.post('https://tehnologii-web-alexandrapavelescu.c9users.io/get-all', product).then((res) => {
            if(res.status === 200){
                this.props.productAdded(product)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }
        
    render(){
        return(
            <div>
                <h1>Add Todo</h1>
                <input type="number" placeholder="Id" 
                    onChange={this.handleChangeId}
                    value={this.state.id} />
                <input type="text" placeholder="Product name" 
                    onChange={this.handleChangeProductName}
                    value={this.state.productName} />
                <input type="number" value={this.state.price}
                    onChange={this.handleChangePrice}
                    value={this.state.price} />
                <button onClick={this.handleAddClick}>Add Product</button>
            </div>
            );
    }
}