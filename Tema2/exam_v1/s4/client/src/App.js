import React, { Component } from 'react';
import './App.css';

import {AddProduct} from './Components/AddProduct';
import {ProductList} from './Components/ProductList';

class App extends Component {
  
  constructor(props){
    super(props);
    this.state = {};
    this.state.products = [];
  }
  
  onProductAdded = (product) => {
    let products = this.state.products;
    products.push(product);
    this.setState({
      products: products
    });
  }
  
  componentWillMount(){
    const url = 'https://tehnologii-web-alexandrapavelescu.c9users.io/get-all'
    fetch(url).then((res) => {
      return res.json();
    }).then((products) =>{
      this.setState({
        products: products
      })
    })
  }
  render() {
    return (
      <React.Fragment>
        <AddProduct productAdded={this.onProductAdded}/>
        <ProductList title="Products" source={this.state.products} />
      </React.Fragment>
    );
  }
}

export default App;
